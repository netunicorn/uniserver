#pragma once
#include <string>
#include <functional>
#include <exception>



namespace Unicorn {
#include <unicorn/unicorn.h>

class Error {
    uc_err err;

public:
    uc_err operator =(const uc_err new_err);

    uc_err get() const {
        return err;
    }
};

class Exception : public std::exception {
public:
    Error error;

    Exception(const Error error) : error(error) {}
    virtual const char *what() const throw() {
        return uc_strerror(error.get());
    }
};

class Engine;

using Callback = std::function<void (Engine, uint64_t)>;
void cb_wrap(uc_engine *uc, uint64_t data, Callback *real_cb);

class Engine {
    uc_engine *uc;
    Error err;
    bool toDestruct = true;
    std::vector<Callback*> cbs;

public:
    Engine(uc_arch arch, uc_mode mode) {
        err = uc_open(arch, mode, &uc);
    }
    Engine(uc_engine *uc) : uc(uc) {
        toDestruct = false;
    }
    ~Engine() {
        if (toDestruct) {
            err = uc_close(uc);
            for (auto cb : cbs) delete cb;
        }
    }

    auto get_underlaying() {
        return uc;
    }

    auto query(uc_query_type type) {
        size_t fres;
        err = uc_query(uc, type, &fres);
        return fres;
    }

    template<typename T>
    void reg_write(int regid, const T &value) {
        err = uc_reg_write(uc, regid, reinterpret_cast<const void*>(&value));
    }

    template<typename T>
    void reg_read(int regid, T &value) {
        err = uc_reg_read(uc, regid, reinterpret_cast<void*>(&value));
    }

    template<typename T>
    void reg_write_batch(int *regs, T *const *vals, int count) {
        err = uc_reg_write_batch(uc, regs, reinterpret_cast<void* const*>(vals), count);
    }

    template<typename T>
    void reg_read_batch(int *regs, T **vals, int count) {
        err = uc_reg_read_batch(uc, regs, reinterpret_cast<void**>(vals), count);
    }

    template<typename T>
    void mem_write(uint64_t address, const T *bytes, size_t size = 0) {
        err = uc_mem_write(uc, address, reinterpret_cast<const void*>(bytes), size?size:sizeof(T));
    }

    template<typename T>
    void mem_read(uint64_t address, T *bytes, size_t size = 0) {
        err = uc_mem_read(uc, address, reinterpret_cast<void*>(bytes), size?size:sizeof(T));
    }

    void emu_start(uint64_t begin, uint64_t until, uint64_t timeout, size_t count) {
        err = uc_emu_start(uc, begin, until, timeout, count);
    }

    void emu_stop() {
        err = uc_emu_stop(uc);
    }

    template<typename insT>
    uc_hook hook_add(int type, Callback callback, insT instruction, uint64_t begin = 1, uint64_t end = 0) {
        uc_hook hh;
        auto cb = new Callback(std::move(callback));
        cbs.push_back(cb);
        err = uc_hook_add(uc, &hh, type, reinterpret_cast<void*>(cb_wrap), reinterpret_cast<void*>(cb), begin, end, instruction);
        return hh;
    }

    void hook_del(uc_hook hh) {
        err = uc_hook_del(uc, hh);
    }

    void mem_map(uint64_t address, size_t size, uint32_t perms) {
        err = uc_mem_map(uc, address, size, perms);
    }

    void mem_unmap(uint64_t address, size_t size) {
        err = uc_mem_unmap(uc, address, size);
    }

    void mem_protect(uint64_t address, size_t size, uint32_t perms) {
        err = uc_mem_protect(uc, address, size, perms);
    }

    void context_alloc(uc_engine *uc, uc_context **context) {
        err = uc_context_alloc(uc, context);
    }

    void free(void *mem) {
        err = uc_free(mem);
    }

    void context_save(uc_context& context) {
        err = uc_context_save(uc, &context);
    }

    void context_restore(uc_context& context) {
        err = uc_context_restore(uc, &context);
    }

    size_t context_size() {
        return uc_context_size(uc);
    }

    void context_free(uc_context& context) {
        err = uc_context_free(&context);
    }
};
}
