# Uniserver
This is the netunicorn server. It allows you to let other services or people run x86_64 code on your computer safely.

## First step
Make sure to clone all submodules:

    git submodule update --init --recursive --depth 1

## Adding API commands
To add your own API commands first edit unibuild/api.h then add its functionallity to main.cpp

## Building
    mkdir build &&
    cd build &&
    cmake .. &&
    make

## Network data format
    // Pseudo code
    struct {
        uint64_t size;
        char flat_binary[size];
    };
