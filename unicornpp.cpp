#include "unicorn.hpp"
#include <functional>

namespace Unicorn {
void cb_wrap(uc_engine *uc, uint64_t data, Callback *real_cb) {
    if (reinterpret_cast<uint64_t>(real_cb) < 10) {
        real_cb = reinterpret_cast<Callback*>(data);
    }
    (*real_cb)(Engine(uc), data);
}

uc_err Error::operator =(const uc_err new_err) {
    err = new_err;
    if (err) {
        throw Exception(*this);
    }
    return err;
}
}
