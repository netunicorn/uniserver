#include <iostream>
#include <string>
#include <functional>
#include <mutex>
#include <sstream>
#include <bitset>
#include <cstddef>
#include <cstring>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>




namespace tcplisten {
template<int maxfds = FD_SETSIZE>
class Listener {
public:
    using FDs = std::bitset<FD_SETSIZE>;

    size_t sock;
    FDs fds;
    std::mutex fds_lock;
    std::ostream& logstream;

    FDs sockswitch() {
        FDs fres = fds;
        if (select(maxfds, reinterpret_cast<fd_set*>(&fres), nullptr, nullptr, nullptr) < 0) {
            perror("select");
            exit(EXIT_FAILURE);
        }
        return fres;
    }

    void connect(std::string ip_addr, int thissock) {
        logstream << "Connect from host "
                  << ip_addr
                  << " (" << thissock << ')'
                  << std::endl;
        fds.set(thissock);
    }

    inline void ignore(int thissock) {
        fds.reset(thissock);
    }

    void disconnect(int thissock) {
        logstream << "Disconnect of host "
                  << thissock
                  << std::endl;
        ignore(thissock);
        close(thissock);
    }

    void callback(std::function<void(int fd)> on_message, std::function<void(int fd, const std::string& ip)> after_accept = [] (...) {}) {
        struct sockaddr_in clientname{};
        // Block until input arrives on one or more active sockets
        auto readfds = sockswitch();
        // Service all the sockets with input pending
        for (size_t fd = 0; fd != readfds.size(); fd++) {
            if (readfds.test(fd)) {
                if (fd == sock) {
                    logstream << "Processing accept on socket " << fd << "..." << std::endl;
                    // Accept connection request on original socket
                    socklen_t addrlen = sizeof(clientname);
                    int newsock = accept(sock,
                                         reinterpret_cast<struct sockaddr *>(&clientname),
                                         &addrlen);
                    if (newsock < 0) {
                        perror("accept");
                        exit(EXIT_FAILURE);
                    }
                    // Connect to client
                    connect(inet_ntoa(clientname.sin_addr), newsock);
                    // Callback
                    after_accept(newsock, inet_ntoa(clientname.sin_addr));
                } else {
                    logstream << "Processing input on socket " << fd << "..." << std::endl;
                    // Data arriving on an already-connected socket
                    on_message(fd);
                }
            }
        }
    }

    Listener(int port, std::ostream& logstream = std::clog)
        : logstream(logstream) {
        static_assert (maxfds <= FD_SETSIZE, "Socket bitset would overflow with that amount of maximum FDs");
        // Create the socket and set it up to accept connections
        sock = make_socket(port);
        if (listen(sock, 1) < 0) {
            perror("listen");
            exit(EXIT_FAILURE);
        }
        // Add to fds
        fds.set(sock);
    }


private:
    int make_socket(uint16_t port) {
        int sock;
        struct sockaddr_in name{};
        // Create the socket
        sock = socket(PF_INET, SOCK_STREAM, 0);
        if (sock < 0) {
            perror("socket");
            exit(EXIT_FAILURE);
        }
        // Give the socket a name
        name.sin_family = AF_INET;
        name.sin_port = htons(port);
        name.sin_addr.s_addr = htonl(INADDR_ANY);
        if (bind(sock, reinterpret_cast<struct sockaddr *>(&name), sizeof(name)) < 0) {
            perror("bind");
            exit(EXIT_FAILURE);
        }
        // Return created socket
        return sock;
    }
};
}
