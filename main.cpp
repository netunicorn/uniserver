#include <iostream>
#include <string>
#include <thread>
#include <sstream>
#include <cstring>
#include <sys/signal.h>
#include <sys/epoll.h>
#include "connection.hpp"
#include "unicorn.hpp"

using namespace Unicorn;

namespace API {
#include "unibuild/api.h"
}

constexpr API::mem_info memConfig = {
    .executable_adress = 0x0000000000000000,
    .stack_adress = 0x1000000000000000,
    .heap_adress = 0x8000000000000000,
    .executable_size = 1 * 1024 * 1024, // 1 MB
    .stack_size = 32 * 1024, // 32 KB
    .heap_size = 4 * 1024 * 1024 // 4 MB
};


void write_error(int fd, const std::string& msg) {
    auto str = "E: "+msg + '\n';
    write(fd, str.c_str(), str.size());
}

void on_read(int fd) {
    // Create epoll
    epoll_event ev = {
        .events = EPOLLIN,
        .data = {
            .fd = fd
        }
    };
    int epollfd = epoll_create(1);
    epoll_ctl(epollfd, EPOLL_CTL_ADD, fd, &ev);
    epoll_event epoll_events[1];

    // Read code headers
    uint64_t code_size;
    read(fd, &code_size, sizeof(code_size));

    // Validate size
    if (code_size > memConfig.executable_size || code_size == 0) {
        write_error(fd, "Invalid code size");
        return;
    }

    // Read code
    std::vector<char> code(code_size);
    uint64_t bytes_read = 0;
    while (bytes_read != code_size) {
        if (epoll_wait(epollfd, epoll_events, 1, 10000) <= 0) {
            write_error(fd, "Epoll wait error");
            return;
        }
        ssize_t status;
        bytes_read += status = read(fd, code.data() + bytes_read, code_size - bytes_read);
        if (status <= 0) {
            write_error(fd, "Read error");
            return;
        }
    }

    // Initialize emulator in X86-64bit mode
    Engine uc(UC_ARCH_X86, UC_MODE_64);

    // Map memory
    uc.mem_map(memConfig.executable_adress, memConfig.executable_size, UC_PROT_ALL);
    uc.mem_map(memConfig.stack_adress, memConfig.stack_size, UC_PROT_ALL);

    // Write machine code to be emulated to executable memory
    uc.mem_write(memConfig.executable_adress, code.data(), code_size);

    // Add hook
    bool heap_enabled = false;
    uc.hook_add(UC_HOOK_INSN, [&heap_enabled, fd] (Engine engine, uint64_t) {
        // Get command
        API::cmd_t cmd_id, data, size;
        engine.reg_read(UC_X86_REG_RAX, cmd_id);
        engine.reg_read(UC_X86_REG_RBX, data);
        engine.reg_read(UC_X86_REG_RDX, size);

        // Run
        switch (cmd_id) {
        using namespace API;
        case CMD_EXIT: {
            engine.emu_stop();
            break;
        }
        case CMD_MEMINFO: {
            engine.mem_write(data, &memConfig);
            break;
        }
        case CMD_HEAP_ENABLE: {
            if (!heap_enabled) {
                engine.mem_map(memConfig.heap_adress, memConfig.heap_size, UC_PROT_ALL);
                heap_enabled = true;
            }
            break;
        }
        case CMD_HEAP_DISABLE: {
            if (heap_enabled) {
                engine.mem_unmap(memConfig.heap_adress, memConfig.heap_size);
                heap_enabled = false;
            }
            break;
        }
        case CMD_PRINT: {
            if (size != 0) {
                std::vector<char> buf(size);
                engine.mem_read(data, buf.data(), size);
                write(fd, buf.data(), size);
            }
            break;
        }
        }
    }, UC_X86_INS_SYSCALL);

    // Initialize stack
    uc.reg_write(UC_X86_REG_RSP, memConfig.stack_adress + memConfig.stack_size);

    // Emulate code in infinite time & unlimited instructions
    try {
        uc.emu_start(memConfig.executable_adress, memConfig.executable_adress + sizeof(code) - 1, 10000000/*10s*/, 100000);
    }  catch (std::exception& e) {
        write_error(fd, e.what());
    }
}



int main(int argc, char **argv) {
    std::string port = "1138";
    if (argc > 1) {
        port = argv[1];
    }

    signal(SIGPIPE, SIG_IGN);
    tcplisten::Listener server(std::stoi(port));

    while (true) {
        server.callback([&server] (int fd) {
            server.ignore(fd);
            std::thread t([&server, fd] () {
                on_read(fd);
                server.disconnect(fd);
            });
            t.detach();
        });
    }
}
